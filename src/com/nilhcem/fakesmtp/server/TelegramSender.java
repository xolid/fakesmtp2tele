/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nilhcem.fakesmtp.server;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author czarate
 */
public class TelegramSender {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(TelegramSender.class);
    
    public static void sendToTelegram(String chatid, String apitoken, String mensaje) {
        
        String urlString = "https://api.telegram.org/bot"+apitoken+"/sendMessage?chat_id="+chatid+"&text="+mensaje+"&parse_mode=html";

        try {
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            InputStream is = new BufferedInputStream(conn.getInputStream());
        } catch (IOException e) {
            LOGGER.error("No se pudo enviar el mensaje por telegram.");
        }
        
    }
    
}
